include config.mk

BIN  = $(NAME)
SRC  = $(NAME).c
MAN  = $(NAME).1
DIST = $(NAME)-$(VERSION)
TGZ  = $(DIST).tar.gz

all: $(BIN)

$(BIN): $(SRC)
	@echo CC $<
	@$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o $@ $<
	@strip $@

clean:
	@echo cleaning
	@rm -f $(BIN) $(TGZ)

distribute: clean
	@echo creating distribution tarball
	@mkdir -p $(DIST)
	@cp LICENSE README Makefile config.mk $(MAN) $(SRC) $(DIST)
	@tar -czf $(TGZ) $(DIST)
	@rm -rf $(DIST)

install: $(BIN) $(MAN)
	@echo installing binary to $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/$(BIN)
	@echo installing manual page to $(DESTDIR)$(MANPREFIX)/man1
	@mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	@sed "s/VERSION/$(VERSION)/g" < $(MAN) > $(DESTDIR)$(MANPREFIX)/man1/$(MAN)
	@chmod 644 $(DESTDIR)$(MANPREFIX)/man1/$(MAN)

uninstall:
	@echo removing binary from $(DESTDIR)$(PREFIX)/bin
	@rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN)
	@echo removing manual page from $(DESTDIR)$(MANPREFIX)/man1
	@rm -f $(DESTDIR)$(MANPREFIX)/man1/$(MAN)
