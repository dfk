dfk
===

`dfk` is a is a daemonised file queue. It maintains a list of pathnames
and passes each in turn using to external program.

Requirements
------------

A C compiler.

On Linux, [libbsd](http://libbsd.freedesktop.org/) and
[pkg-config](http://pkg-config.freedesktop.org/).

Installation
------------

Edit “config.mk” to your liking, then run

    make install

On Linux, `dfk` must be linked with libbsd:

    make install LDFLAGS="$(pkg-config --static --libs libbsd-ctor)"

Usage
-----

Refer to the program's manual page for usage information.

Author
------

Evan Hanson <evhan@foldling.org>

See “LICENSE” for copyright information.
