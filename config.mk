NAME = dfk
VERSION = 0.3

PROGRAM = mpg123

PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

CFLAGS = -Wall -pedantic -Os -static
LDFLAGS =
CPPFLAGS = -DVERSION=\"$(VERSION)\" -DNAME=\"$(NAME)\" -DPROGRAM=\"$(PROGRAM)\"
