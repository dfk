#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef __linux__
# include <bsd/unistd.h>
#endif

typedef struct item {
  char *s;
  struct item *n;
} item;

int fifo;
int pid;
int count;
int quiet;

char *program;
char *queue;

item *head;
item *tail;

void clear(int);
void next(int);
void stop(int);
void take(int);

#define chomp(s) (s[strlen(s) - 1] = '\0', s)
#define die(s) (err(errno, "%s", s))
#define getenvd(s, d) (getenv(s) ? getenv(s) : d)
#define action(f) (action.sa_handler = f, &action)
#define title(n) setproctitle("%d", n)

struct sigaction action;

void sigenable(void) {
  action.sa_flags = SA_RESTART | SA_NOCLDSTOP;
  sigaction(SIGHUP,  action(clear), NULL);
  sigaction(SIGINT,  action(next),  NULL);
  sigaction(SIGTERM, action(stop),  NULL);
  sigaction(SIGCHLD, action(take),  NULL);
}

void sigdisable(void) {
  signal(SIGHUP,  SIG_DFL);
  signal(SIGINT,  SIG_DFL);
  signal(SIGTERM, SIG_DFL);
  signal(SIGCHLD, SIG_DFL);
}

void enqueue(char *s) {
  item *t = malloc(sizeof(item));
  t->s = strdup(s);
  t->n = NULL;
  if (!head)
    head = tail = t;
  else
    tail = tail->n = t;
  title(++count);
}

char *dequeue() {
  item *n;
  char *s;
  if (!head)
    return NULL;
  n = head;
  s = head->s;
  head = head->n;
  free(n);
  title(--count);
  return s;
}

void clear(int n) {
  char *s;
  while ((s = dequeue()))
    free(s);
}

void add(char *s) {
  if (strlen(s) < 2)
    warnx("invalid command: %s", s);
  else
    enqueue(s + 2);
}

void next(int n) {
  if (pid) {
    kill(pid, SIGTERM);
    waitpid(pid, NULL, 0);
  }
}

void stop(int n) {
  sigdisable();
  next(n);
  if (remove(queue) < 0)
    die(queue);
  exit(EXIT_SUCCESS);
}

void take(int n) {
  char *s;
  sigdisable();
  waitpid(pid, NULL, 0);
  if (!(s = dequeue()))
    stop(0);
  if (!(pid = fork())) {
    if (quiet) close(1), close(2);
    execlp(program, program, s, NULL);
    die(program);
  } else {
    sigenable();
    free(s);
  }
}

int command(FILE *f) {
  char b[BUFSIZ];
  if (fgets(b, sizeof(b), f) == NULL)
    return 0;
  switch (*chomp(b)) {
    case 'a':
      add(b);
      return 1;
    case 'c':
      clear(0);
      return 1;
    case 'n':
      next(0);
      return 1;
    case 'q':
      quiet = !quiet;
      return 1;
    case 'k':
      stop(0);
    default:
      return 0;
  }
}

void start(void) {
  int fd;
  FILE *f;

  if (mkfifo(queue, S_IRUSR | S_IWUSR | S_IXUSR) < 0)
    if (errno != EEXIST)
      die(queue);

  if ((fd = open(queue, O_RDWR)) < 0)
    die(queue);

  if (fork()) {
    close(fd);
    wait(NULL);
  } else if (fork()) {
    exit(EXIT_SUCCESS);
  } else {
    close(fifo);
    sigenable();
    if (!(f = fdopen(fd, "r")))
      die(queue);
    while (command(f))
      if (!pid) take(0);
  }
}

int connect(void) {
  int fd;
  struct stat st;

  if (stat(queue, &st) > -1 && !S_ISFIFO(st.st_mode))
    errx(EXIT_FAILURE, "%s: Invalid fifo", queue);

retry:

  if ((fd = open(queue, O_WRONLY | O_NONBLOCK)) > 0) {
    return fd;
  } else if (errno == ENOENT || errno == ENXIO) {
    start();
    goto retry;
  } else {
    die(queue);
  }
}

void send(char c, char *s) {
  int n;
  char b[BUFSIZ];
  if (!fifo)
    fifo = connect();
  if ((n = snprintf(b, BUFSIZ, "%c %s\n", c, s)) < 0)
    die(queue);
  write(fifo, b, n);
}

void file(char *s) {
  char p[PATH_MAX];
  if (access(s, R_OK) < 0 || realpath(s, p) == NULL)
    warn("%s", s);
  else
    send('a', p);
}

void init(char buf[]) {
  char *env;
  char *tmpdir = getenvd("TMPDIR", "/tmp");
  program = getenvd("DFK_PROGRAM", PROGRAM);
  if ((env = getenv("DFK_FIFO")))
    queue = env;
  else if (snprintf(buf, PATH_MAX, "%s/%s.dfk", tmpdir, getenv("USER")) > 0)
    queue = buf;
  else
    die(queue);
}

void option(char *arg) {
  int i;
  char c;
  for (i = 1; (c = arg[i]); i++) {
    switch (c) {
      case 'c':
      case 'k':
      case 'n':
      case 'q':
        send(c, "");
        break;
      case 'v':
        puts(NAME " " VERSION);
        exit(EXIT_SUCCESS);
      default:
        errx(EXIT_FAILURE, "usage: " NAME " [-v] [-cknq] [file ...]");
    }
  }
}

int main(int argc, char *argv[]) {
  int i;
  char b[PATH_MAX], q[PATH_MAX];

  init(q);

  if (argc == 1 && isatty(0))
    exit(access(q, R_OK));

  for (i = 1; i < argc; i++) {
    if (*argv[i] == '-')
      option(argv[i]);
    else
      file(argv[i]);
  }

  if (!isatty(0)) {
    while (fgets(b, sizeof(b), stdin))
      file(chomp(b));
  }

  exit(EXIT_SUCCESS);
}
